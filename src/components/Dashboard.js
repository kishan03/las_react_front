import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

class Dashboard extends Component {
    state = {
        redirectToLogin: true,
    }
    render () {
        const { redirectToLogin } = this.state;
        if (redirectToLogin) {
            return <Redirect to="/login" />
        }
        return (
            <h1>Dashboard</h1>
        )
    }
}

export default Dashboard;