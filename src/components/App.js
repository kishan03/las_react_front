import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import '../static/css/App.css';
import Dashboard from './Dashboard';
import Login from './Login';


class App extends Component {
  render() {
    return (
        <Router>
            <div>
                <Route path="/" exact component={Dashboard} />
                <Route path="/login" component={Login} />
            </div>
        </Router>
    );
  }
}

// function mapStateToProps({authedUser}) {
//   return { loading: authedUser === null }
// }

export default App;
