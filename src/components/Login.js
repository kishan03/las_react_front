import React, { Component } from "react";
import '../static/css/App.css';
import '../static/css/animation.css';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';


const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',   
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
    },
    button: {
        margin: theme.spacing.unit,
    }
});


class Login extends Component {
    state = {
        "email": "test@email.com",
        "password": "password",
    }
    
    handleChange = name => event => {
        console.log(event.target.value);
        this.setState({ [name]: event.target.value });
    };
    render () {
        const { classes } = this.props;
        const { email, password } = this.state;
        return (
            <div className="container">
                <center>
                    <h1>Login To L.A.S</h1>
                </center>
                <center className="animated animatedFadeInUp fadeInUp">
                    <form method="POST" className={classes.container} action=".">
                        <Grid container spacing={24}>
                            <Grid item xs={12}>
                                <TextField 
                                    className={classes.textField}
                                    margin="normal"
                                    value={email}
                                    onChange={this.handleChange('email')}
                                    label="Name" />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    className={classes.textField}
                                    type="password"
                                    value={password}
                                    onChange={this.handleChange('password')}
                                    label="Password" />
                            </Grid>
                            <Grid item xs={12}>
                                <Button variant="contained" color="primary" className={classes.button}>Submit</Button>
                            </Grid>
                        </Grid>
                    </form>
                </center>
            </div>
        )
    }
}

Login.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(Login);